/*********************
 * binaryConverter.c
 * By; F. Stephen Quaratiello
 *
 * A simple program to convert an ascii string of 1's and 0's into that string
 * in actual binary, or to convert actual binary into a string of 1's and 0's.
 * 
 * A trivial problem, but a fun one to mess around with.
 *
 * No Rights Reserved
 ***********************/

#include <stdio.h>

void decode(FILE *input, FILE *output);
void encode(FILE *input, FILE *output);

int main(int argc, char* argv[]){
	//usage definition
	if(argc != 4){
		printf("The proper usage is: biconv <e/d> <infile> <outfile>. \nTo get input from stdin, use - for the infile\n");
		exit(0);
	}
	else{ 
		char flag = getopt(argc, argv, "ed"); //to determine if we are Encoding or Decoding
		FILE *input = stdin;
		if(argv[2] != '-'){
			input = fopen(argv[2], "r");
			if(input == 0){
				printf("Could not open file");
				exit(0);
			}
		}
		FILE *output = fopen(argv[3], "w");
		if(flag == 'e'){ //if we are encoding, encode
			encode(input,output);
		}
		else{ //catch-all case for decoding.
			decode(input,output);
		}
	fclose(output);
	fclose(input);
	}
}

/*******************************
 * For our purposes, decoding
 * refers to converting binary
 * data into an ascii string of
 * 1's and 0's...
 ********************************/

void decode(FILE *input,FILE *output){
	while(1){
		int x = fgetc(input);
		if(x==EOF)break;
		int num;
		for(num = 0; num<8;num++){
			if((char)(x<<num)>>8){ //fun bit-shifting
				fputc('1', output);
			}
			else{
				fputc('0',output);
			}
		}
	}
	return;
}

//... And encoding refers to the inverse.

void encode(FILE *input,FILE *output){
	unsigned char holder = 0;
	int num = 0;
	while(1){
		int x = fgetc(input);
		if(x == EOF) break;
		holder <<= 1;
		if(x == '1'){
			holder = holder | 1; //bitwise opperations are *fun*.
		}
		num++;
		if(num%8 == 0){
			fputc(holder, output);
			holder = 0;
		}
	}
	if(num%8 !=0) fputc(holder, output); //catching an annoying edge case
	return;
}		
